from database import DATABASE_PATH
import csv
from .models import CallRecord

FIELDNAMES = ['id', 'name', 'email', 'password', 'age']


class CallService():
    @staticmethod
    def all_users():
        with open(DATABASE_PATH) as repository:
            reader = csv.DictReader(repository)
            return list(reader)
    @staticmethod
    def get_by_id(user_id):
        users = CallService.all_users()
        for item in users:
            if int(item['id']) == int(user_id):
                return item

    @staticmethod
    def signup(**kwargs):
        call_record = CallRecord(**kwargs)
        all = CallService.all_users()
        if(kwargs['email'] in [email['email'] for email in all]):
            
            return {"erro": "email invalido"},201
        with open(DATABASE_PATH, 'a') as repository:
            writer = csv.DictWriter(repository, fieldnames=FIELDNAMES)
            if all:
                kwargs["id"] = int(max([item['id'] for item in all])) + 1
            else:
                kwargs["id"] = 1

            writer.writerow(kwargs)
            return kwargs
    @staticmethod
    def login(**kwargs):
        for item in CallService.all_users():
            if (item['email'] == kwargs['email'] and item['password'] == kwargs['password']):
                return {
                    "id": item['id'],
                    "name": item['name'],
                    "email": item['email'],
                    "age": item['age']
                }
        return {}
    @staticmethod
    def update_user(user_id, **kwargs):
        tipo = list(kwargs.keys())[0]
        for item in CallService.all_users():
            if int(item['id']) == int(user_id):
                 user = item
        for item in kwargs:
            user[item] = kwargs[item]

        data = [item for item in CallService.all_users() if int(item['id']) != int(user_id)]
        data.append(user)
        

        with open(DATABASE_PATH, 'w') as repository:

            writer = csv.DictWriter(repository, fieldnames=FIELDNAMES)
            writer.writeheader()
            writer.writerows(data)
        return {}
    @staticmethod
    def delete(user_id):
        data = [item for item in CallService.all_users() if int(item['id']) != int(user_id)]
        with open(DATABASE_PATH, 'w') as repository:

            writer = csv.DictWriter(repository, fieldnames=FIELDNAMES)
            writer.writeheader()
            writer.writerows(data)
        return {}
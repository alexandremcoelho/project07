from flask import jsonify, request
from .services import CallService

FIELDNAMES = ['call_id', 'origin', 'destination', 'start', 'end']

def register_call_views(app):
    @app.route('/users')
    def users():
        calls = CallService.all_users()
        return jsonify(calls)
    @app.route('/signup', methods=['POST'])
    def signup():
        try:
            
            call_record = CallService.signup(**request.json)
            return {
                'id': call_record['id'],
                'name': call_record['name'],
                'email': call_record['email'],
                'age': call_record['age']
                }, 201
        except:
            return {},422

    @app.route('/login', methods=['POST'])
    def login():
        data = CallService.login(**request.json)
        if data:
            return data
        return {'erro': 'dados incorretos'}, 400
        

    @app.route('/profile/<user_id>', methods=['PATCH'])
    def update_user(user_id):
        CallService.update_user(user_id, **request.json)
        user = CallService.get_by_id(user_id)
        if user:
            return {
                'id': user['id'],
                'name': user['name'],
                'email': user['email'],
                'age': user['age']
                }, 201
        return ('', 404)
    @app.route('/profile/<user_id>', methods=['DELETE'])
    def delete_user(user_id):
        user = CallService.get_by_id(user_id)
        CallService.delete(user_id)
        if user:
            return '',204
        return ('', 404)


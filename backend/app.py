from flask import Flask
from .calls.views import register_call_views

app = Flask(__name__)

register_call_views(app)
